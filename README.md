# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [WN's shop]
* Key functions (add/delete)
    1. [procduct page]
    2. [shopping pipeline]
    3. [user dashboard]
* Other functions (add/delete)
    1. [商品搜尋]
    2. [商品篩選(上身/下身/配件...)]
    3. [商品排序(最新/價格高到低/價格低到高)]
    4. [賣家可以上傳更新product page的banner picture]
    5. [當買家重複點擊相同商品,購物車會自動更新商品數量]
    6. [買家可以在購物車內隨意更新商品數量或是取消購買商品]
    7. [購物車價格總額會隨購物車內商品內容動態更新]
    8. [pipeline在買家最後送出訂單前都可以回上一步更改購買資訊]
    9. [賣家可以在dashboard更新交易狀態]
    10. [買家可以在dashboard刪除訂單紀錄(僅限當交易完成時)]
    11. [賣家可以在dashboard刪除訂單紀錄]
    12. [用戶都可以自己上傳更新自己的大頭貼]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description

衣物購物網站,一個賣家對多個買家,採會員制,只有會員才可以購買商品

**公佈賣家帳號密碼方便助教之後自己測試
**賣家帳號:saller@gmail.com
**賣家密碼:saller
(一開始拼錯字之後就將錯就錯)

使用到Firebase database API,Firebase storage API, Firebase auth API, notifications API

1. [index] 主頁面,當使用者登出時便跳回主頁
2. [shop] 顯示所有商品,可以搜尋商品,依不同方式排序商品,依不同類別顯示商品/若為賣家可上架新商品以及更新banner圖片
3. [shopping] shopping pipeline,確認購物清單,買家資料以及付款方式,最後送出訂單
4. [dashboard] 顯示使用者的交易紀錄,若交易完成可以刪除紀錄/若為賣家則顯示所有購物訂單,並可以更新交易進度
5. [singnin] 利用email或是第三方登入
6. [signup] 利用email和passsword註冊帳號

## Security Report (Optional)

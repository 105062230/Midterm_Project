function init(){
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            if(user_email=='saller@gmail.com'){
                document.getElementById('buyer').style.display = 'none';
                document.getElementById('saller').style.display = 'block';
            }
            else {
                document.getElementById('saller').style.display = 'none';
                document.getElementById('buyer').style.display = 'block';
            }
            menu.innerHTML = "<span class='dropdown-item' id='dash-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)

            document.getElementById('username').innerHTML = user_email;
            
            var logout_btn = document.getElementById("logout-btn");
            logout_btn.addEventListener('click',function(){
                firebase.auth().signOut().then(function(result){
                    alert('success');
                    window.location.href="index.html";
                }).catch(function(error){
                    alert('error');
                });
            });

            var dash_btn =  document.getElementById("dash-btn");
            dash_btn.addEventListener('click',function(){
                window.location.href = "dashboard.html";
            });
            
            
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
        }
    });


    var dashRef = firebase.database().ref('dashlist');
    var total_post = [];
    str_before='<div class="buying-list-item d-flex align-items-center"><div class="list-btn"><button class="btn-remove" id="';
    str_before_s1='<div class="buying-list-item d-flex align-items-center"><div class="list-btn3"><button class="btn-remove" id="';
    str_before_s2='<div class="list-btn3"><button class="btn-ok" id="';
    str_after_id='" onclick="deletelist(this.id)">X</button></div><div class="time"><h5>'
    str_after_id_s1='" onclick="deletelist(this.id)">X</button></div>'
    str_after_id_s2='" onclick="checklist(this.id)">V</button></div><div class="time2"><h5>'
    str_after_time='</h5></div><div class="money"><h5>NT ';
    str_after_money='</h5></div><div class="pay-way"><h5>';
    str_after_payway='</h5></div><div class="item-state"><h5>';
    str_after_time_s='</h5></div><div class="money2"><h5>NT ';
    str_after_money_s='</h5></div><div class="pay-way2"><h5>';
    str_after_payway_s='</h5></div><div class="item-state2"><h5>';
    str_after='</h5></div></div>'

    dashRef.on('value', function(snapshot){
        total_post.length = 0;
        snapshot.forEach(function(childsnapshot){
            if(firebase.auth().currentUser.email=='saller@gmail.com'){
                total_post[total_post.length] = str_before_s1 + childsnapshot.key + str_after_id_s1 + str_before_s2 + childsnapshot.key + str_after_id_s2 + childsnapshot.val().Checkout_time + str_after_time_s + childsnapshot.val().Checkout_money + str_after_money_s + childsnapshot.val().Checkout_way + str_after_payway_s + childsnapshot.val().state + str_after;
            }
            else{
                if(firebase.auth().currentUser.email==childsnapshot.val().user_email){
                    total_post[total_post.length] = str_before + childsnapshot.key + str_after_id + childsnapshot.val().Checkout_time + str_after_time + childsnapshot.val().Checkout_money + str_after_money + childsnapshot.val().Checkout_way + str_after_payway + childsnapshot.val().state + str_after;
                }
            }
        });
        if(firebase.auth().currentUser.email=='saller@gmail.com')document.getElementById("dash-list2").innerHTML = total_post.join('');
        else document.getElementById("dash-list").innerHTML = total_post.join('');
    });


    var uploadFileInput = document.getElementById("uploadFileInput");
    var storageRef = firebase.storage().ref();
    uploadFileInput.addEventListener('change', function(){
        var file = uploadFileInput.files[0];
        upload_pic(file);
    },false);

}

function upload_pic(file){
        var storageRef = firebase.storage().ref();
        var user_email = firebase.auth().currentUser.email;
        var uploadTask = storageRef.child('user-pic/'+file.name).put(file);
        uploadTask.on('state_changed', function(snapshot){

        // 取得檔案上傳狀態，並用數字顯示

            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'

                console.log('Upload is paused');
                break;
            case firebase.storage.TaskState.RUNNING: // or 'running'

                console.log('Upload is running');
                break;
            }
        }, function(error) {
            // Handle unsuccessful uploads

        }, function() {
            // Handle successful uploads on complete

            // For instance, get the download URL: https://firebasestorage.googleapis.com/...

            var downloadURL = uploadTask.snapshot.downloadURL;
            
            var inputRef = firebase.database().ref('user-pic');
            newpost = inputRef.push();
            newpost.set({
                    email: user_email,
                    pic: downloadURL
            }).then(function(){
                clean_last_pic(user_email);
            })
            .catch(e => console.log(e.message));
      });
}

function clean_last_pic(user_email){
    var inputRef = firebase.database().ref('user-pic');
    var key1 = 0;
    var key2 = 0;
    inputRef.on('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            if(user_email==childsnapshot.val().email){
                key2 = key1;
                key1 = childsnapshot.key;
            }
        });
        if(key2!=0)inputRef.child(key2).remove();
    });
}

function deletelist(key){
    var dashRef = firebase.database().ref('dashlist');

    dashRef.on('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            if(key==childsnapshot.key){
                if(childsnapshot.val().state=='已出貨')dashRef.child(key).remove().then(
                );
                else alert('交易尚未完成 無法刪除紀錄');
            }
        });
    });
}

function checklist(key){
    var dashRef = firebase.database().ref('dashlist');

    dashRef.on('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            if(key==childsnapshot.key){
                var updates = {};
                updates['dashlist/' + key + '/state'] = '已出貨';
                firebase.database().ref().update(updates);
            }
        });
    });
}

function init_pic(){
    var picRef = firebase.database().ref('user-pic');
    var total_post = [];
    var count = 0;
    str_before2='<img class="info-pic" src="';
    str_after2='">'

    picRef.on('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            count += 1;
            if(firebase.auth().currentUser.email==childsnapshot.val().email){
                total_post[0] = str_before2 + childsnapshot.val().pic + str_after2;
            }
            else total_post[0] = '<img class="info-pic" src="https://openclipart.org/image/2400px/svg_to_png/196720/mono-business-user.png">'
        });
        if(count==0)total_post[0] = '<img class="info-pic" src="https://openclipart.org/image/2400px/svg_to_png/196720/mono-business-user.png">'
        document.getElementById("userpic").innerHTML = total_post.join('');
    });
}

window.onload = function () {
    init();
    init_pic();
};
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='dash-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)

            var logout_btn = document.getElementById("logout-btn");
            logout_btn.addEventListener('click',function(){
                firebase.auth().signOut().then(function(result){
                    alert('success');
                }).catch(function(error){
                    alert('error');
                });
            });

            var dash_btn =  document.getElementById("dash-btn");
            dash_btn.addEventListener('click',function(){
                window.location.href = "dashboard.html";
            });
            
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
        }
    });
};

window.onload = function () {
    init();
};
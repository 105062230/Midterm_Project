var f = 'all';

function init() {
    init_banner();
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var post_set = document.getElementById('post-set');
        var banner_set =  document.getElementById('banner_set');
        var car = document.getElementById('car');       // Check user login
        if (user) {
            if(user.email=='saller@gmail.com'){
                post_set.style.display = 'block';
                banner_set.style.display = 'block';
                car.style.display = 'none';
            }
            else car.style.display = 'block';
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='dash-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)

            var logout_btn = document.getElementById("logout-btn");
            logout_btn.addEventListener('click',function(){
                firebase.auth().signOut().then(function(result){
                    alert('success');
                }).catch(function(error){
                    alert('error');
                });
            });

            var dash_btn =  document.getElementById("dash-btn");
            dash_btn.addEventListener('click',function(){
                window.location.href = "dashboard.html";
            });

            
            
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            post_set.style.display = 'none';
            banner_set.style.display = 'none';
            car.style.display = 'none';
        }
    });

    post_btn = document.getElementById('post_btn');
    post_name = document.getElementById('goodsname');
    post_money = document.getElementById('money');
    post_sty = document.getElementById('StyleControlSelect1');
    var uploadFileInput = document.getElementById("uploadFileInput");
    var uploadFileInput2 = document.getElementById("uploadFileInput2");
    var storageRef = firebase.storage().ref();
    
    post_btn.addEventListener('click', function(){
        var file = uploadFileInput.files[0];
        if(post_name.value != ""&&post_money.value != ""&&file!="")var uploadTask = storageRef.child('saller/'+file.name).put(file);
        else alert('請填入完整資訊');
        uploadTask.on('state_changed', function(snapshot){

        // 取得檔案上傳狀態，並用數字顯示

            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'

                console.log('Upload is paused');
                break;
            case firebase.storage.TaskState.RUNNING: // or 'running'

                console.log('Upload is running');
                break;
            }
        }, function(error) {
            // Handle unsuccessful uploads

        }, function() {
            // Handle successful uploads on complete

            // For instance, get the download URL: https://firebasestorage.googleapis.com/...

            var downloadURL = uploadTask.snapshot.downloadURL;
            var inputRef = firebase.database().ref('good_list');
            var n = Math.floor(Math.random() * (500 + 1));
            newpost = inputRef.push();
            newpost.set({
                    goodname: post_name.value,
                    goodmoney: post_money.value,
                    style: post_sty.value,
                    clothid: 'c' + n,
                    pic: downloadURL
            }).catch(e => console.log(e.message));
            document.getElementById('goodsname').value = '';
            document.getElementById('money').value = '';
            document.getElementById('uploadFileInput').value = '';
      });
    },false);

    // The html code for post
    var str_before_pic = '<div class="good column" id="';
    var str_after_id = '"onclick="getid(this)"><img src="'
    var str_after_pic = '" class="goodimg"><div class="goodinf"><h3 class="goodtit">';

    var str_after_name = '</h3><h3 class="goodpri">'
    var str_after_money = '</h3></div></div>'

    var postsRef = firebase.database().ref('good_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            snapshot.forEach(function(childsnapshot) {
                var childdata = childsnapshot.val();
                total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                first_count +=1 ;
            });
            document.getElementById("good_post").innerHTML = total_post.join('');
            
            postsRef.on('child_added', function(data){
                second_count += 1;
                if(second_count > first_count){
                    var childdata = data.val();
                    total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                    document.getElementById("good_post").innerHTML = total_post.join('');
                }
            });

        })
        .catch(e => console.log(e.message));
};


function getid(x){ 
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            var clothRef = firebase.database().ref('good_list').orderByChild('clothid');
            clothRef.once('value', function(snapshot){
                snapshot.forEach(function(childsnapshot){
                    var childdata = childsnapshot.val();
                    if(x.id==childdata.clothid&&user_email!='saller@gmail.com'){
                        
                        var inputRef = firebase.database().ref('buylist');

                        var notification = new Notification('成功加入購物車', {
                            body: childdata.goodname + ' 已加入購物車'
                        });
                        notification.onclick = function(e) { // 綁定點擊事件
                            e.preventDefault(); // prevent the browser from focusing the Notification's tab
                            window.location.href = 'shopping.html'; // 打開特定網頁
                        }
                        newpost = inputRef.push();
                        newpost.set({
                                user_email:user_email,
                                good:childdata,
                                num:0
                        }).catch(e => console.log(e.message));
                        check_same(x.id, childdata, user_email);
                    }
                });
            });
            
        } else {
            var notification2 = new Notification("WN's 提醒", {
                body: '欲購買商品請先登入/註冊會員'
            });
            notification2.onclick = function(e) { // 綁定點擊事件
                e.preventDefault(); // prevent the browser from focusing the Notification's tab
                window.location.href = 'signin.html'; // 打開特定網頁
            }
        }
    });
}

function check_same(id, x, user){
    var inputRef = firebase.database().ref('buylist').orderByChild('good/clothid');
    var count = 0;
    inputRef.once('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            if(id==childsnapshot.val().good.clothid){
                count += 1;
                firebase.database().ref('buylist').child(childsnapshot.key).remove();
                
                console.log(id + ' ' + childsnapshot.val().good.clothid);
            }
        });
        newpost = firebase.database().ref('buylist').push();
            newpost.set({
                user_email:user,
                good:x,
                num:count
            }).catch(e => console.log(e.message));
        console.log(count);

    });
    
}

function init_banner(){
    var uploadFileInput = document.getElementById("uploadFileInput2");
    var storageRef = firebase.storage().ref();
    uploadFileInput.addEventListener('change', function(){
        var file = uploadFileInput.files[0];
        if(file!="")var uploadTask = storageRef.child('banner/'+file.name).put(file);
        uploadTask.on('state_changed', function(snapshot){

        // 取得檔案上傳狀態，並用數字顯示

            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'

                console.log('Upload is paused');
                break;
            case firebase.storage.TaskState.RUNNING: // or 'running'

                console.log('Upload is running');
                break;
            }
        }, function(error) {
            // Handle unsuccessful uploads

        }, function() {
            // Handle successful uploads on complete

            // For instance, get the download URL: https://firebasestorage.googleapis.com/...

            var downloadURL = uploadTask.snapshot.downloadURL;
            var inputRef = firebase.database().ref('banner');
            newpost = inputRef.push();
            newpost.set({
                pic: downloadURL
            }).catch(e => console.log(e.message));

            document.getElementById('uploadFileInput').value = '';
            window.location.reload();
      });
    },false);

    var postsRef = firebase.database().ref('banner');

    var str_before_pic = '<img src="';
    var str_after_pic = '" class="coverimg">'
    var banner_pic = [];
    var key1 = 0;
    var key2 = 0;
    
    

    postsRef.once('value')
        .then(function (snapshot) {
            var count = 0;
            snapshot.forEach(function(childsnapshot) {
                var childdata = childsnapshot.val();
                banner_pic[0] = str_before_pic + childdata.pic + str_after_pic;
                key2 = childsnapshot.key;
                count += 1;
            });
            document.getElementById("banner_space").innerHTML = banner_pic.join('');
            if(count>0)postsRef.child(key1).remove().then(
                console.log('delet '+key1)
            );
        })
        .catch(e => console.log(e.message))
        
        
}

function init_filter(){
    var btn_new = document.getElementById('new');
    var btn_sale = document.getElementById('sale');
    var btn_up = document.getElementById('up');
    var btn_down = document.getElementById('down');
    var btn_acc = document.getElementById('acc');
    var btn_all = document.getElementById('all');

    btn_all.addEventListener('click',function(){
        select('all');
        f = 'all';
        filter('All');
        document.getElementById('ctlgtit').innerHTML = 'All 所有商品';
        document.getElementById('newsort').style.display = 'block';
        document.getElementById('low').style.display = 'block';
        document.getElementById('high').style.display = 'block';
    });

    btn_new.addEventListener('click',function(){
        filter('New');
        select('new');
        f = 'New';
        document.getElementById('newsort').style.display = 'none';
        document.getElementById('low').style.display = 'none';
        document.getElementById('high').style.display = 'none';
    });
    btn_sale.addEventListener('click',function(){
        filter('連身 / 套裝');
        select('sale');
        f = '連身 / 套裝';
        document.getElementById('newsort').style.display = 'block';
        document.getElementById('low').style.display = 'block';
        document.getElementById('high').style.display = 'block';
    });
    btn_up.addEventListener('click',function(){
        filter('上身');
        select('up');
        f = '上身';
        document.getElementById('newsort').style.display = 'block';
        document.getElementById('low').style.display = 'block';
        document.getElementById('high').style.display = 'block';
    });
    btn_down.addEventListener('click',function(){
        filter('下身');
        select('down');
        f = '下身';
        document.getElementById('newsort').style.display = 'block';
        document.getElementById('low').style.display = 'block';
        document.getElementById('high').style.display = 'block';
    });
    btn_acc.addEventListener('click',function(){
        filter('配件');
        select('acc');
        f = '配件';
        document.getElementById('newsort').style.display = 'block';
        document.getElementById('low').style.display = 'block';
        document.getElementById('high').style.display = 'block';
    });
}

function filter(x){
    // The html code for post
    var str_before_pic = '<div class="good column" id="';
    var str_after_id = '"onclick="getid(this)"><img src="'
    var str_after_pic = '" class="goodimg"><div class="goodinf"><h3 class="goodtit">';

    var str_after_name = '</h3><h3 class="goodpri">'
    var str_after_money = '</h3></div></div>'

    var postsRef = firebase.database().ref('good_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    if(x=='New') {
        document.getElementById('ctlgtit').innerHTML = 'New Arrival';
        postsRef.limitToLast(8).once('value')
            .then(function (snapshot) {
                snapshot.forEach(function(childsnapshot) {
                    var childdata = childsnapshot.val();
                    total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                    total_post.reverse();
                });
                document.getElementById("good_post").innerHTML = total_post.join('');
            })
            .catch(e => console.log(e.message));
    }
    else if(x=='All'){
        document.getElementById('ctlgtit').innerHTML = 'New Arrival';
        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function(childsnapshot) {
                    var childdata = childsnapshot.val();
                    total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                });
                document.getElementById("good_post").innerHTML = total_post.join('');
                //window.location.reload();
            })
            .catch(e => console.log(e.message));
    }
    else {
        document.getElementById('ctlgtit').innerHTML = x;

        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function(childsnapshot) {
                    var childdata = childsnapshot.val();
                    if(childdata.style==x)total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                });
                document.getElementById("good_post").innerHTML = total_post.join('');
                //window.location.reload();
            })
            .catch(e => console.log(e.message));
    }
}

function select(x){
    var ctlg = document.querySelectorAll(".ctlgitem");
    for (var i = 0; i < ctlg.length; i++){
        ctlg[i].classList.remove("selected");
    }

    document.getElementById(x).classList.add('selected');
}

function init_search(){
    var name = document.getElementById('goodsearch');
    var btn_search = document.getElementById('btn_search');

    btn_search.addEventListener('click',function(){
        document.getElementById('newsort').style.display = 'none';
        document.getElementById('low').style.display = 'none';
        document.getElementById('high').style.display = 'none';
        search(name.value);
    })
}

function search(x){

    var postsRef = firebase.database().ref('good_list');
    
    var str_before_pic = '<div class="good column" id="';
    var str_after_id = '"onclick="getid(this)"><img src="'
    var str_after_pic = '" class="goodimg"><div class="goodinf"><h3 class="goodtit">';

    var str_after_name = '</h3><h3 class="goodpri">'
    var str_after_money = '</h3></div></div>'

    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    document.getElementById('ctlgtit').innerHTML = '搜尋關鍵字 "' + x + '" 的相關商品';

    postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function(childsnapshot) {
                    var childdata = childsnapshot.val();
                    if((childdata.goodname).indexOf(x)!=-1)total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                });
                if(total_post.length==0)total_post[total_post.length] = '沒有相關商品';
                document.getElementById("good_post").innerHTML = total_post.join('');
            })
            .catch(e => console.log(e.message));
}

function init_sort(){
    var btn_newsort = document.getElementById('newsort');
    var btn_low = document.getElementById('low');
    var btn_high = document.getElementById('high');

    btn_newsort.addEventListener('click', function(){
        sort_goods(0);
        select2('newsort');
    });
    btn_low.addEventListener('click', function(){
        sort_goods(1);
        select2('low');
    });
    btn_high.addEventListener('click', function(){
        sort_goods(2);
        select2('high');
    });
}

function select2(x){
    var sort = document.querySelectorAll(".sort");
    for (var i = 0; i < sort.length; i++){
        sort[i].classList.remove("sort-way");
    }

    document.getElementById(x).classList.add('sort-way');
}

function sort_goods(x){
    var postsRef = firebase.database().ref('good_list');
    
    var str_before_pic = '<div class="good column" id="';
    var str_after_id = '"onclick="getid(this)"><img src="'
    var str_after_pic = '" class="goodimg"><div class="goodinf"><h3 class="goodtit">';

    var str_after_name = '</h3><h3 class="goodpri">'
    var str_after_money = '</h3></div></div>'

    var total_post = [];

    if(f=='all'){
        if(x==0){
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childsnapshot) {
                        var childdata = childsnapshot.val();
                        total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                    });
                    total_post.reverse();
                    document.getElementById("good_post").innerHTML = total_post.join('');
                })
                .catch(e => console.log(e.message));
        }
        else if(x==1){
            postsRef.orderByChild('goodmoney').once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childsnapshot) {
                        var childdata = childsnapshot.val();
                        total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                    });
                    total_post.reverse();
                    document.getElementById("good_post").innerHTML = total_post.join('');
                })
                .catch(e => console.log(e.message));
        }
        else if(x==2){
            postsRef.orderByChild('goodmoney').once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childsnapshot) {
                        var childdata = childsnapshot.val();
                        total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                    });
                    document.getElementById("good_post").innerHTML = total_post.join('');
                })
                .catch(e => console.log(e.message));
        }
    }
    else{
        if(x==0){
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childsnapshot) {
                        var childdata = childsnapshot.val();
                        if(childdata.style==f)total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                    });
                    total_post.reverse();
                    document.getElementById("good_post").innerHTML = total_post.join('');
                })
                .catch(e => console.log(e.message));
        }
        else if(x==1){
            postsRef.orderByChild('goodmoney').once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childsnapshot) {
                        var childdata = childsnapshot.val();
                        if(childdata.style==f)total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                    });
                    total_post.reverse();
                    document.getElementById("good_post").innerHTML = total_post.join('');
                })
                .catch(e => console.log(e.message));
        }
        else if(x==2){
            postsRef.orderByChild('goodmoney').once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childsnapshot) {
                        var childdata = childsnapshot.val();
                        if(childdata.style==f)total_post[total_post.length] = str_before_pic + childdata.clothid + str_after_id + childdata.pic + str_after_pic + childdata.goodname + str_after_name + 'NT ' + childdata.goodmoney + str_after_money;
                    });
                    document.getElementById("good_post").innerHTML = total_post.join('');
                })
                .catch(e => console.log(e.message));
        }
    }
}

window.onload = function () {
    init();
    init_filter();
    init_search();
    init_sort();
    if(Notification.permission!='granted')Notification.requestPermission();
};
var total_post = [];
var total_post2 = [];
var total_money = 0;
var total_money2 = 0;
var count = 0;

function init(){
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='dash-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)

            var logout_btn = document.getElementById("logout-btn");
            logout_btn.addEventListener('click',function(){
                firebase.auth().signOut().then(function(result){
                    alert('success');
                    window.location.href = 'index.html';
                }).catch(function(error){
                    alert('error');
                });
            });

            var dash_btn =  document.getElementById("dash-btn");
            dash_btn.addEventListener('click',function(){
                window.location.href = "dashboard.html";
            });

            
            
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
        }
    });

    var btn_next = document.getElementById('btn_next');
    btn_next.addEventListener('click',function(){
        if(total_money!=0){
            document.getElementById('part2').style.display='block';
            document.getElementById('part1').style.display='none';
        }
        else alert('購物車內沒有商品 無法結帳');
    });


    var btn_goback = document.getElementById('btn_goback');
    btn_goback.addEventListener('click',function(){
        window.location.href = "shop.html";
    });

    var buyer_name = document.getElementById('buyer-name');
    var buyer_phone = document.getElementById('buyer-phone');
    var buyer_add = document.getElementById('buyer-add');

    var btn_back1 = document.getElementById('btn_back1');

    btn_back1.addEventListener('click',function(){
        document.getElementById('part2').style.display='none';
        document.getElementById('part1').style.display='block';
    });

    var btn_next2 = document.getElementById('btn_next2');
    btn_next2.addEventListener('click',function(){
        if(buyer_name.value=='')alert('請填入完整資訊');
        else if(buyer_phone.value=='')alert('請填入完整資訊');
        else if(buyer_add.value=='')alert('請填入完整資訊');
        else{
           document.getElementById('part2').style.display='none';
            document.getElementById('part3').style.display='block';
        }
    });

    var btn_back2 = document.getElementById('btn_back2');

    btn_back2.addEventListener('click',function(){
        document.getElementById('part3').style.display='none';
        document.getElementById('part2').style.display='block';
    });

    var btn_checkout = document.getElementById('btn_checkout');  
    btn_checkout.addEventListener('click',function(){

        if(document.querySelector('input[name="exampleRadios"]:checked').value == '匯款'&&(document.getElementById('buyer-bank').value==''||document.getElementById('buyer-acc').value=='')){
            alert('請輸入匯款完整資訊');
        }
        else{
            list_checkout();
            document.getElementById('part3').style.display='none';
            document.getElementById('part4').style.display='block';

            var goodRef = firebase.database().ref('buylist');

            goodRef.once('value', function(snapshot){
                snapshot.forEach(function(childsnapshot){
                    if(firebase.auth().currentUser.email==childsnapshot.val().user_email){
                        loadgoodlist(childsnapshot.val().good, childsnapshot.val().num);
                    }
                });
                total_post2[total_post2.length] = '<div id="counting"><b>Total : </b> NT ' + total_money2 + '</div>';
                document.getElementById("buy-list-check").innerHTML = total_post2.join('');
            })
        }
        
    });

    var btn_finish1 = document.getElementById('btn_finish1');
    btn_finish1.addEventListener('click',function(){
        deletelist();
        window.location.href = "shop.html";
    });

    var btn_finish3 = document.getElementById('btn_finish3');
    btn_finish3.addEventListener('click',function(){
        deletelist();
        window.location.href = "dashboard.html";
    });

    var goodRef = firebase.database().ref('buylist');

    goodRef.on ('value', function(snapshot){
        total_post.length = 0;
        total_money = 0;
        snapshot.forEach(function(childsnapshot){
            if(firebase.auth().currentUser.email==childsnapshot.val().user_email){
                findtheGoods(childsnapshot.val().good, childsnapshot.val().num, childsnapshot.key);
            }
        });
        total_post[total_post.length] = '<div id="counting"><b>Total : </b> NT ' + total_money + '</div>';
        
        document.getElementById("the-list").innerHTML = total_post.join('');
    });

}

function findtheGoods(x,n,k){

    str_before = '<div class="d-flex list-item align-items-center"><div class="item-btn"><button class="list-item-remove" id="'
    str_after_id = '" onclick="deletegoods(this.id)">x</button></div><div class="list-item-img"><img class="img" src="';
    str_before2 = '<div class="d-flex list-item2 align-items-center""><div class="list-item-img"><img class="img" src="';
    str_after_pic = '" alt=""></div><div class="list-item-name">';
    str_after_name = '</div><div class="list-item-price">NT ';
    str_after_money = '</div><div class="list-item-amount d-flex justify-content-between"><button class="amount-minus" id="' + k + '" onclick="minus(this.id)">-</button><input type="text" class="amount form-control" value="'
    str_after_num = '"><button class="amount-plus" id="'
    str_after_plus = '" onclick="plus(this.id)">+</button></div></div>';
    str_after_money2 = '</div><div class="list-item-amount d-flex justify-content-between">件</div></div>';

    childsnapshot = x;
    
    total_post[total_post.length] = str_before + childsnapshot.clothid + str_after_id + childsnapshot.pic + str_after_pic + childsnapshot.goodname + str_after_name + childsnapshot.goodmoney + str_after_money + n + str_after_num + k + str_after_plus;
    count += 1;
    total_money += (parseInt(childsnapshot.goodmoney))*n;
}

function loadgoodlist(x,n){
    childsnapshot = x;
    str_before2 = '<div class="d-flex list-item2 align-items-center""><div class="list-item-img"><img class="img" src="';
    str_after_pic = '" alt=""></div><div class="list-item-name">';
    str_after_name = '</div><div class="list-item-price">NT ';
    str_after_money2 = '</div><div class="list-item-amount d-flex justify-content-between">'
    str_after_num2 = '件</div></div>';
    total_post2[total_post2.length] = str_before2 + childsnapshot.pic + str_after_pic + childsnapshot.goodname + str_after_name + childsnapshot.goodmoney + str_after_money2 + n + str_after_num2;
    total_money2 += (parseInt(childsnapshot.goodmoney))*n;
}

function list_checkout(){
    var buyer_name = document.getElementById('buyer-name').value;
    var buyer_phone = document.getElementById('buyer-phone').value;
    var buyer_add = document.getElementById('buyer-add').value;
    var Checkout_way = document.querySelector('input[name="exampleRadios"]:checked').value;
    var today = new Date;
    var Checkout_time = (today.getMonth()+ 1) + '/' + today.getDate() + ' ' + today.getHours() + ':' + today.getMinutes();

    document.getElementById('buyer-name2').value = buyer_name;
    document.getElementById('buyer-phone2').value = buyer_phone;
    document.getElementById('buyer-add2').value = buyer_add;
    document.getElementById('Checkout_way').value = Checkout_way;

    var user = firebase.auth().currentUser.email;
    var inputRef = firebase.database().ref('dashlist');
    newpost = inputRef.push();
    newpost.set({
            user_email:user,
            buyer_name:buyer_name,
            buyer_phone:buyer_phone,
            buyer_add:buyer_add,
            Checkout_way:Checkout_way,
            Checkout_time:Checkout_time,
            Checkout_money:total_money,
            total_post:total_post2,
            state:'商品準備中'
    }).catch(e => console.log(e.message));

    total_money = 0;
    count = 0;
    total_post = [];
    total_post2 = [];
    
}

function deletegoods(x){
    var goodRef = firebase.database().ref('buylist');
    
    goodRef.on('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            var childdata = childsnapshot.val();
            if(firebase.auth().currentUser.email==childdata.user_email&&x==childdata.good.clothid){
                alert('取消購買 ' + childdata.good.goodname);
                goodRef.child(childsnapshot.key).remove().then(console.log('delete'));
            }
        });
    });
}

function deletelist(){
    var goodRef = firebase.database().ref('buylist');
    
    goodRef.on('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            var childdata = childsnapshot.val();
            if(firebase.auth().currentUser.email==childdata.user_email){
                goodRef.child(childsnapshot.key).remove().then(console.log('delete'));
            }
        });
    });
}

function minus(k){
    var goodRef = firebase.database().ref('buylist');
    console.log('minus ' + k);
    goodRef.once('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            if(k==childsnapshot.key){
                var n = childsnapshot.val().num - 1;  
                if(n>0){
                    var updates = {};
                    updates['buylist/' + k + '/num'] = n;
                    firebase.database().ref().update(updates);
                }             
                else goodRef.child(k).remove();
            }
        });
    });
}

function plus(k){
    var goodRef = firebase.database().ref('buylist');
    console.log('plus ' + k);
    goodRef.once('value', function(snapshot){
        snapshot.forEach(function(childsnapshot){
            if(k==childsnapshot.key){
                var n = childsnapshot.val().num + 1;               
                var updates = {};
                updates['buylist/' + k + '/num'] = n;
                firebase.database().ref().update(updates);
            }
        });
    });
}

window.onload = function () {
    init();
};
function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnSignUp = document.getElementById('btnSignUp');

    var uploadFileInput = document.getElementById("uploadFileInput");
    var storageRef = firebase.storage().ref();


    btnSignUp.addEventListener('click', function () {        
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field

        var email = txtEmail.value;
        var password = txtPassword.value;
        var pic = '';
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function (result) {
                firebase.auth().signInWithEmailAndPassword(email, password);
                alert("Welcome!");
                window.location.href = 'index.html';
                
        }).catch(function (error) {
            create_alert('error',error.message);
            txtEmail.value= '';
            txtPassword.value= '';
        });

    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};